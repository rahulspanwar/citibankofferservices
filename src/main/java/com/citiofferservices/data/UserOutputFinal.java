package com.citiofferservices.data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 418315
 *
 */
public class UserOutputFinal {

	
	
	private  Integer geoOfferId;
	private List<CitiBankOffers>  listCitiBankOffers;
	private String geoCoordinate;
	private String responseStatus;
	
	
	public String getResponseStatus() {
		return responseStatus;
	}
	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}
	public String getGeoCoordinate() {
		return geoCoordinate;
	}
	public void setGeoCoordinate(String geoCoordinate) {
		this.geoCoordinate = geoCoordinate;
	}
	public List<CitiBankOffers> getListCitiBankOffers() {
		return listCitiBankOffers;
	}
	public void setListCitiBankOffers(List<CitiBankOffers> listCitiBankOffers) {
		this.listCitiBankOffers = listCitiBankOffers;
	}
	
	public Integer getGeoOfferId() {
		return geoOfferId;
	}
	public void setGeoOfferId(Integer geoOfferId) {
		this.geoOfferId = geoOfferId;
	}
	
	@Override
	public String toString() {
		return "UserOutputFinal [geoOfferId=" + geoOfferId + ", listCitiBankOffers=" + listCitiBankOffers
				+ ", geoCoordinate=" + geoCoordinate + "]";
	}
	
	
}
