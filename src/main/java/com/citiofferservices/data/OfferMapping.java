package com.citiofferservices.data;

import java.util.ArrayList;
import java.util.List;

public class OfferMapping {
	private Integer recordId;
	private String geoCoordinate;
	private Double radius;
	private Integer geoOfferId;
	private List<GeoCoordinate> geoCoordinateList;
	ArrayList<String> geoCoordArray;
	private String polygonCoordinate;
	
	

	
	
	public String getPolygonCoordinate() {
		return polygonCoordinate;
	}
	public void setPolygonCoordinate(String polygonCoordinate) {
		this.polygonCoordinate = polygonCoordinate;
	}
	public Integer getRecordId() {
		return recordId;
	}
	public void setRecordId(Integer recordId) {
		this.recordId = recordId;
	}
	public String getGeoCoordinate() {
		return geoCoordinate;
	}
	public void setGeoCoordinate(String geoCoordinate) {
		this.geoCoordinate = geoCoordinate;
	}
	
	public Double getRadius() {
		return radius;
	}
	public void setRadius(Double radius) {
		this.radius = radius;
	}
	public Integer getGeoOfferId() {
		return geoOfferId;
	}
	public void setGeoOfferId(Integer geoOfferId) {
		this.geoOfferId = geoOfferId;
	}
	
	public List<GeoCoordinate> getGeoCoordinateList() {
		return geoCoordinateList;
	}
	public void setGeoCoordinateList(List<GeoCoordinate> geoCoordinateList) {
		this.geoCoordinateList = geoCoordinateList;
	}
	
	public ArrayList<String> getGeoCoordArray() {
		return geoCoordArray;
	}
	public void setGeoCoordArray(ArrayList<String> geoCoordArray) {
		this.geoCoordArray = geoCoordArray;
	}
	@Override
	public String toString() {
		return "OfferMapping [recordId=" + recordId + ", geoCoordinate=" + geoCoordinate + ", radius=" + radius
				+ ", geoOfferId=" + geoOfferId + ", geoCoordinateList=" + geoCoordinateList + ", geoCoordArray="
				+ geoCoordArray + ", polygonCoordinate=" + polygonCoordinate + "]";
	}
	
	
	
}
