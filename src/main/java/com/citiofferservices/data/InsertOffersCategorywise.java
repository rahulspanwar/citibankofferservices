package com.citiofferservices.data;

import java.util.List;

public class InsertOffersCategorywise {
	private String geoCoordinates;
	private Integer radius;
	private List<CitiBankOffers> list;
	private String day ;
	private String conveyance ;
	private String weather ;
	private String timeRange ;
	public String getGeoCoordinates() {
		return geoCoordinates;
	}
	public void setGeoCoordinates(String geoCoordinates) {
		this.geoCoordinates = geoCoordinates;
	}
	public Integer getRadius() {
		return radius;
	}
	public void setRadius(Integer radius) {
		this.radius = radius;
	}
	public List<CitiBankOffers> getList() {
		return list;
	}
	public void setList(List<CitiBankOffers> list) {
		this.list = list;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getConveyance() {
		return conveyance;
	}
	public void setConveyance(String conveyance) {
		this.conveyance = conveyance;
	}
	public String getWeather() {
		return weather;
	}
	public void setWeather(String weather) {
		this.weather = weather;
	}
	public String getTimeRange() {
		return timeRange;
	}
	public void setTimeRange(String timeRange) {
		this.timeRange = timeRange;
	}
	@Override
	public String toString() {
		return "MerchantTriggeredOffers [geoCoordinates=" + geoCoordinates + ", radius=" + radius + ", list=" + list
				+ ", day=" + day + ", conveyance=" + conveyance + ", weather=" + weather + ", timeRange=" + timeRange
				+ "]";
	}
	
}
