package com.citiofferservices.data;

public class Coordinates {
	private Double srcLatitude ;
	private Double desLatitude ;
	private Double srcLongitude ;
	private Double desLongitude ;
	private Double srcHeight ;
	private Double desHeight ;
	private String distance ;
	private String responseStatus;


	public String getResponseStatus() {
		return responseStatus;
	}
	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}
	public Double getSrcLatitude() {
		return srcLatitude;
	}
	public void setSrcLatitude(Double srcLatitude) {
		this.srcLatitude = srcLatitude;
	}
	public Double getDesLatitude() {
		return desLatitude;
	}
	public void setDesLatitude(Double desLatitude) {
		this.desLatitude = desLatitude;
	}
	public Double getSrcLongitude() {
		return srcLongitude;
	}
	public void setSrcLongitude(Double srcLongitude) {
		this.srcLongitude = srcLongitude;
	}
	public Double getDesLongitude() {
		return desLongitude;
	}
	public void setDesLongitude(Double desLongitude) {
		this.desLongitude = desLongitude;
	}
	public Double getSrcHeight() {
		return srcHeight;
	}
	public void setSrcHeight(Double srcHeight) {
		this.srcHeight = srcHeight;
	}
	public Double getDesHeight() {
		return desHeight;
	}
	public void setDesHeight(Double desHeight) {
		this.desHeight = desHeight;
	}
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	@Override
	public String toString() {
		return "Coordinates [srcLatitude=" + srcLatitude + ", desLatitude=" + desLatitude + ", srcLongitude="
				+ srcLongitude + ", desLongitude=" + desLongitude + ", srcHeight=" + srcHeight + ", desHeight="
				+ desHeight + ", distance=" + distance + ", responseStatus=" + responseStatus + "]";
	}



}
