package com.citiofferservices.data;

public class OfferCardMapping {
	
	private Integer offerCardMapId;
	private String cardId;
	private Integer offerId;
	private String createdBy;
	private String createdDate;
	private String updatedBy;
	private String updatedDate;
	public Integer getOfferCardMapId() {
		return offerCardMapId;
	}
	public void setOfferCardMapId(Integer offerCardMapId) {
		this.offerCardMapId = offerCardMapId;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public Integer getOfferId() {
		return offerId;
	}
	public void setOfferId(Integer offerId) {
		this.offerId = offerId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	@Override
	public String toString() {
		return "OfferCardMapping [offerCardMapId=" + offerCardMapId + ", cardId=" + cardId + ", offerId=" + offerId
				+ ", createdBy=" + createdBy + ", createdDate=" + createdDate + ", updatedBy=" + updatedBy
				+ ", updatedDate=" + updatedDate + "]";
	}
	
	

}
