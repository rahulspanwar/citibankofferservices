package com.citiofferservices.data;

public class GeoOfferMapping {
	private Integer recordId;
	private Integer geoOfferId;
	private Integer offerId;
	
	
	public Integer getRecordId() {
		return recordId;
	}
	public void setRecordId(Integer recordId) {
		this.recordId = recordId;
	}
	public Integer getGeoOfferId() {
		return geoOfferId;
	}
	public void setGeoOfferId(Integer geoOfferId) {
		this.geoOfferId = geoOfferId;
	}
	public Integer getOfferId() {
		return offerId;
	}
	public void setOfferId(Integer offerId) {
		this.offerId = offerId;
	}
	@Override
	public String toString() {
		return "GeoOfferMapping [recordId=" + recordId + ", geoOfferId=" + geoOfferId + ", offerId=" + offerId + "]";
	}
	
	
}
