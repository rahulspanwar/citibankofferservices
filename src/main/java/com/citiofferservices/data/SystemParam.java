package com.citiofferservices.data;

import java.io.Serializable;

public class SystemParam implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer recordId;
	private String sysParam;
	private String value;
	public Integer getRecordId() {
		return recordId;
	}
	public void setRecordId(Integer recordId) {
		this.recordId = recordId;
	}
	public String getSysParam() {
		return sysParam;
	}
	public void setSysParam(String sysParam) {
		this.sysParam = sysParam;
	}
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "SystemParam [recordId=" + recordId + ", sysParam=" + sysParam + ", value=" + value + "]";
	}
	
	

}
