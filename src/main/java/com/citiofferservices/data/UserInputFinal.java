package com.citiofferservices.data;

import java.util.Date;
import java.util.List;

public class UserInputFinal {

	
	private GeoCoordinate geoCoordinate;
	private String day;
	private String conveyance;
	private List<String> weather;
	private String date;
	
	
	
   

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getConveyance() {
		return conveyance;
	}

	public void setConveyance(String conveyance) {
		this.conveyance = conveyance;
	}

	

	public List<String> getWeather() {
		return weather;
	}

	public void setWeather(List<String> weather) {
		this.weather = weather;
	}

	public GeoCoordinate getGeoCoordinate() {
		return geoCoordinate;
	}

	public void setGeoCoordinate(GeoCoordinate geoCoordinate) {
		this.geoCoordinate = geoCoordinate;
	}

	@Override
	public String toString() {
		return "UserInputFinal [geoCoordinate=" + geoCoordinate + ", day=" + day + ", conveyance=" + conveyance
				+ ", weather=" + weather + ", date=" + date + "]";
	}

	
	

}
