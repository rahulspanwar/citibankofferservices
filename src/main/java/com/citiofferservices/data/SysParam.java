package com.citiofferservices.data;

public class SysParam {
	private Integer recordId;
	private String apiName;
	private String apiKey;
	public Integer getRecordId() {
		return recordId;
	}
	public void setRecordId(Integer recordId) {
		this.recordId = recordId;
	}
	public String getApiName() {
		return apiName;
	}
	public void setApiName(String apiName) {
		this.apiName = apiName;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	@Override
	public String toString() {
		return "SysParam [recordId=" + recordId + ", apiName=" + apiName + ", apiKey=" + apiKey + "]";
	}
	
	
	
}
