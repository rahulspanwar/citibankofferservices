package com.citiofferservices.data;

import java.util.ArrayList;
import java.util.List;

public class APIKeys {

	
	List<String> apiKeys = new ArrayList<String>();

	public List<String> getApiKeys() {
		return apiKeys;
	}

	public void setApiKeys(List<String> apiKeys) {
		this.apiKeys = apiKeys;
	}

	@Override
	public String toString() {
		return "APIKeys [apiKeys=" + apiKeys + "]";
	}
	
	 
}
