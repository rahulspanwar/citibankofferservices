package com.citiofferservices.data;

import java.util.List;

public class LoginDetails {
	private Integer userPin;
	private Integer customerId;
	private Integer merchantId;
	private String merchantName;
	private String imgUrl;
	private List<CitiBankOffers> offerList;
	private String message;
	private Boolean status;
	
	
	
	public Integer getUserPin() {
		return userPin;
	}
	public void setUserPin(Integer userPin) {
		this.userPin = userPin;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public List<CitiBankOffers> getOfferList() {
		return offerList;
	}
	public void setOfferList(List<CitiBankOffers> offerList) {
		this.offerList = offerList;
	}
	public Integer getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(Integer merchantId) {
		this.merchantId = merchantId;
	}
	@Override
	public String toString() {
		return "LoginDetails [custometId=" + customerId + ", merchantId=" + merchantId + ", merchantName=" + merchantName
				+ ", imgUrl=" + imgUrl + ", offerList=" + offerList + ", message=" + message + ", status=" + status
				+ "]";
	}
	
	
	
	
	
}
