package com.citiofferservices.data;

import java.util.List;

public class GeoOfferDetails {
	private String geoCoordinates;
	private Double radius;
	private List<CitiBankOffers> list;
	private String day ;
	private List<String> conveyanceList ;
	private List<String> weatherList ;
	private String timeRange ;
	private List<String> polygonValues;
	
	
	
	public List<String> getPolygonValues() {
		return polygonValues;
	}
	public void setPolygonValues(List<String> polygonValues) {
		this.polygonValues = polygonValues;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	
	public List<String> getConveyanceList() {
		return conveyanceList;
	}
	public void setConveyanceList(List<String> conveyanceList) {
		this.conveyanceList = conveyanceList;
	}
	public List<String> getWeatherList() {
		return weatherList;
	}
	public void setWeatherList(List<String> weatherList) {
		this.weatherList = weatherList;
	}
	public String getTimeRange() {
		return timeRange;
	}
	public void setTimeRange(String timeRange) {
		this.timeRange = timeRange;
	}
	public String getGeoCoordinates() {
		return geoCoordinates;
	}
	public void setGeoCoordinates(String geoCoordinates) {
		this.geoCoordinates = geoCoordinates;
	}
	
	public Double getRadius() {
		return radius;
	}
	public void setRadius(Double radius) {
		this.radius = radius;
	}
	public List<CitiBankOffers> getList() {
		return list;
	}
	public void setList(List<CitiBankOffers> list) {
		this.list = list;
	}
	@Override
	public String toString() {
		return "GeoOfferDetails [geoCoordinates=" + geoCoordinates + ", radius=" + radius + ", list=" + list + ", day="
				+ day + ", conveyanceList=" + conveyanceList + ", weatherList=" + weatherList + ", timeRange="
				+ timeRange + ", polygonValues=" + polygonValues + "]";
	}
	
	
	
}
