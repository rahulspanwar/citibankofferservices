package com.citiofferservices.data;

public class ResponseMessage {
	private String message;
	private Boolean status;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "ResponseMessage [message=" + message + ", status=" + status + "]";
	}
 }
