package com.citiofferservices.data;

import java.io.Serializable;

public class CitiBankOffers implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Integer offerId;
	private String offerDescription;
	private String imgUrl;
	private String offerCategory;
	private String offerValue ;
	private Double longitude;
	private Double lattitude;
	private Integer merchantId;
	private String companyName ;
	private String address;
	private String offerDate;
	private String averageRating;
	private String totalRating;
	private String createdDate;
	private String createdBy;
	private String updatedDate;
	private String updatedBy;
	private String distance;
	private String locality ;
	private String city ;
	private String actualCity ;
	private String responseStatus;




	public String getResponseStatus() {
		return responseStatus;
	}
	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}
	public String getActualCity() {
		return actualCity;
	}
	public void setActualCity(String actualCity) {
		this.actualCity = actualCity;
	}
	public Integer getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(Integer merchantId) {
		this.merchantId = merchantId;
	}
	public String getAverageRating() {
		return averageRating;
	}
	public void setAverageRating(String averageRating) {
		this.averageRating = averageRating;
	}
	public String getTotalRating() {
		return totalRating;
	}
	public void setTotalRating(String totalRating) {
		this.totalRating = totalRating;
	}
	
	public String getLocality() {
		return locality;
	}
	public void setLocality(String locality) {
		this.locality = locality;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getOfferValue() {
		return offerValue;
	}
	public void setOfferValue(String offerValue) {
		this.offerValue = offerValue;
	}
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getOfferDate() {
		return offerDate;
	}
	public void setOfferDate(String offerDate) {
		this.offerDate = offerDate;
	}
	public Integer getOfferId() {
		return offerId;
	}
	public void setOfferId(Integer offerId) {
		this.offerId = offerId;
	}
	public String getOfferDescription() {
		return offerDescription;
	}
	public void setOfferDescription(String offerDescription) {
		this.offerDescription = offerDescription;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getOfferCategory() {
		return offerCategory;
	}
	public void setOfferCategory(String offerCategory) {
		this.offerCategory = offerCategory;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Double getLattitude() {
		return lattitude;
	}
	public void setLattitude(Double lattitude) {
		this.lattitude = lattitude;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	@Override
	public String toString() {
		return "CitiBankOffers [offerId=" + offerId + ", offerDescription=" + offerDescription
				+ ", imgUrl=" + imgUrl + ", offerCategory=" + offerCategory + ", offerValue=" + offerValue
				+ ", longitude=" + longitude + ", lattitude=" + lattitude + ", merchantId=" + merchantId
				+ ", companyName=" + companyName + ", address=" + address + ", offerDate=" + offerDate
				+ ", averageRating=" + averageRating + ", totalRating=" + totalRating + ", createdDate=" + createdDate
				+ ", createdBy=" + createdBy + ", updatedDate=" + updatedDate + ", updatedBy=" + updatedBy
				+ ", distance=" + distance + ", locality=" + locality + ", city=" + city + ", actualCity=" + actualCity
				+ ", responseStatus=" + responseStatus + "]";
	}



}
