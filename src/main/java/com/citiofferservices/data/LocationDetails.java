package com.citiofferservices.data;

public class LocationDetails {
	private String category;
	private String coordinates;
	private String cardId;
	private String location;
	private String area;
	private Integer offerId;
	private String statusMsg;
	private String actualCity;
	private String country;
	private String city;
	private String merchantName;


	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getActualCity() {
		return actualCity;
	}

	public void setActualCity(String actualCity) {
		this.actualCity = actualCity;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public Integer getOfferId() {
		return offerId;
	}

	public void setOfferId(Integer offerId) {
		this.offerId = offerId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}

	@Override
	public String toString() {
		return "LocationDetails [category=" + category + ", coordinates=" + coordinates + ", cardId=" + cardId
				+ ", location=" + location + ", area=" + area + ", offerId=" + offerId + ", statusMsg=" + statusMsg
				+ ", actualCity=" + actualCity + ", country=" + country + ", city=" + city + ", merchantName="
				+ merchantName + "]";
	}

}
