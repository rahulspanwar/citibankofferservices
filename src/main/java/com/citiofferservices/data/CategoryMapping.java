package com.citiofferservices.data;

public class CategoryMapping {
	private Integer recordId;
	private Integer geoOfferId;
	private String categoryType;
	private String categoryValue;
	
	
	public CategoryMapping() {
		super();
	}
	
	public CategoryMapping(Integer geoOfferId, String categoryType, String categoryValue) {
		super();
		this.geoOfferId = geoOfferId;
		this.categoryType = categoryType;
		this.categoryValue = categoryValue;
	}
	public Integer getRecordId() {
		return recordId;
	}
	public void setRecordId(Integer recordId) {
		this.recordId = recordId;
	}
	public Integer getGeoOfferId() {
		return geoOfferId;
	}
	public void setGeoOfferId(Integer geoOfferId) {
		this.geoOfferId = geoOfferId;
	}
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	public String getCategoryValue() {
		return categoryValue;
	}
	public void setCategoryValue(String categoryValue) {
		this.categoryValue = categoryValue;
	}
	@Override
	public String toString() {
		return "CategoryMapping [recordId=" + recordId + ", geoOfferId=" + geoOfferId + ", categoryType="
				+ categoryType + ", categoryValue=" + categoryValue + "]";
	}
	
	
}
