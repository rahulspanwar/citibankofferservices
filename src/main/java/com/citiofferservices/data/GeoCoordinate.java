package com.citiofferservices.data;

/**
 * @author 418315
 *
 */
public class GeoCoordinate {
	
	
    public String  xCordinate=null;
	public String yCordinate=null;
	public String getxCordinate() {
		return xCordinate;
	}
	public void setxCordinate(String xCordinate) {
		this.xCordinate = xCordinate;
	}
	public String getyCordinate() {
		return yCordinate;
	}
	public void setyCordinate(String yCordinate) {
		this.yCordinate = yCordinate;
	}
	@Override
	public String toString() {
		return "GeoCoordinate [xCordinate=" + xCordinate + ", yCordinate=" + yCordinate + "]";
	}
	
	

	

}
