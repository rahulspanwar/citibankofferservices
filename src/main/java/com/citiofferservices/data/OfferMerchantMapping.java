package com.citiofferservices.data;

//added for merchant offers
public class OfferMerchantMapping {
	private Integer merchantOfferID;
	private Integer merchantID;
	private Integer offerId;
	private String createdBy;
	private String createdDate;
	private String updatedBy;
	private String updatedDate;
	public Integer getMerchantOfferID() {
		return merchantOfferID;
	}
	public void setMerchantOfferID(Integer merchantOfferID) {
		this.merchantOfferID = merchantOfferID;
	}
	public Integer getMerchantID() {
		return merchantID;
	}
	public void setMerchantID(Integer merchantID) {
		this.merchantID = merchantID;
	}
	public Integer getOfferId() {
		return offerId;
	}
	public void setOfferId(Integer offerId) {
		this.offerId = offerId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	@Override
	public String toString() {
		return "OfferMerchantMapping [merchantOfferID=" + merchantOfferID + ", merchantID=" + merchantID + ", offerId="
				+ offerId + ", createdBy=" + createdBy + ", createdDate=" + createdDate + ", updatedBy=" + updatedBy
				+ ", updatedDate=" + updatedDate + "]";
	}
	
	
}
