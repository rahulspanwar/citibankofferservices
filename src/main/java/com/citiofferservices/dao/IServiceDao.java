package com.citiofferservices.dao;

import java.util.List;
import java.util.Map;

import com.citiofferservices.data.CitiBankOffers;
import com.citiofferservices.data.Coordinates;
import com.citiofferservices.data.GeoOfferDetails;
import com.citiofferservices.data.LocationDetails;
import com.citiofferservices.data.LoginDetails;
import com.citiofferservices.data.OfferMapping;
import com.citiofferservices.data.ResponseMessage;
import com.citiofferservices.data.UserInputFinal;
import com.citiofferservices.data.UserOutputFinal;

public interface IServiceDao {

	List<CitiBankOffers> getCitiBankOffers(LocationDetails category, Coordinates coordinates);

	String getSystemParam(String sysParam);

	List<CitiBankOffers> getOffer(LocationDetails locationDetails);

	List<CitiBankOffers> getCitiBankCardOffers(LocationDetails locationDetails, Coordinates coordinates);

	List<CitiBankOffers> getCitiBankAllCardOffers(LocationDetails locationDetails, Coordinates coordinates);

	Map<String, String> getAPIKeys();

	List<CitiBankOffers> getAllOffers();

	List<OfferMapping> getUserDetails(UserInputFinal userInputFinal);

	// added for merchant offers
	List<CitiBankOffers> getMerchantOffers(Integer merchantId);

	LoginDetails authenticateLogin(LoginDetails logindetails);

	ResponseMessage insertOffers(GeoOfferDetails geoOfferdetails);

	UserOutputFinal getOfferIdByGeoCoordinate(UserOutputFinal userOutputFinal);

	List<CitiBankOffers> loadOffers(LoginDetails logindetails);


}
