package com.citiofferservices.dao;

import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Repository;

import com.citiofferservices.data.CategoryMapping;
import com.citiofferservices.data.CitiBankOffers;
import com.citiofferservices.data.Coordinates;
import com.citiofferservices.data.GeoOfferDetails;
import com.citiofferservices.data.GeoOfferMapping;
import com.citiofferservices.data.LocationDetails;
import com.citiofferservices.data.LoginDetails;
import com.citiofferservices.data.MerchantDetails;
import com.citiofferservices.data.OfferCardMapping;
import com.citiofferservices.data.OfferMapping;
import com.citiofferservices.data.OfferMerchantMapping;
import com.citiofferservices.data.ResourceOfferMapping;
import com.citiofferservices.data.ResponseMessage;
import com.citiofferservices.data.SysParam;
import com.citiofferservices.data.SystemParam;
import com.citiofferservices.data.UserInputFinal;
import com.citiofferservices.data.UserOutputFinal;
import com.citiofferservices.utils.CommonDomain;
import com.citiofferservices.utils.MailManager;
import com.citiofferservices.utils.ServiceConstants;
import com.citiofferservices.utils.ServiceLogger;
import com.citiofferservices.utils.Utility;

@Repository
public class ServiceDao implements IServiceDao {

	private static Configuration configuration;
	private static SessionFactory sessionFactory;
	MailManager mailManager = new MailManager();

	/*
	 * @Value("${Offers.folderPath}") String folderPath;
	 *
	 * @Value("${Offers.simplifyscreenfolderPath}") String
	 * simplifyScreenFolderPath;
	 */

	@PostConstruct
	public static void init() {
		configuration = new Configuration();
		configuration.configure(Utility.getConfigurationFile());
		sessionFactory = configuration.buildSessionFactory();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CitiBankOffers> getCitiBankOffers(LocationDetails locationDetails, Coordinates coordinates) {
		ServiceLogger.printInfoLog("Entered getCitiBankOffers");
		Session session = null;
		Transaction transaction = null;
		List<CitiBankOffers> offerList = new ArrayList<>();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			if (locationDetails.getCoordinates() != null && !locationDetails.getCoordinates().equalsIgnoreCase("")) {

				offerList = session.createCriteria(CitiBankOffers.class)
						.add(Restrictions.eq("offerCategory", locationDetails.getCategory()))
						.add(Restrictions.eq("actualCity", locationDetails.getActualCity()))
						.addOrder(Order.desc("offerDate")).list();

				if (offerList.isEmpty()) {
					offerList = session.createCriteria(CitiBankOffers.class)
							.add(Restrictions.eq("offerCategory", locationDetails.getCategory()))
							.addOrder(Order.desc("offerDate")).list();
				}
			} else {
				offerList = session.createCriteria(CitiBankOffers.class)
						.add(Restrictions.eq("offerCategory", locationDetails.getCategory()))
						.add(Restrictions.eq("longitude", 0.0)).add(Restrictions.eq("lattitude", 0.0))
						.add(Restrictions.eq("actualCity", locationDetails.getActualCity()))
						.addOrder(Order.desc("offerDate")).setMaxResults(3).list();

				if (offerList.isEmpty()) {
					offerList = session.createCriteria(CitiBankOffers.class)
							.add(Restrictions.eq("offerCategory", locationDetails.getCategory()))
							.add(Restrictions.eq("longitude", 0.0)).add(Restrictions.eq("lattitude", 0.0))
							.addOrder(Order.desc("offerDate")).setMaxResults(3).list();
				}
			}
			transaction.commit();
			for (CitiBankOffers offers : offerList) {
				offers.setImgUrl(CommonDomain.returnBaseUrl() + offers.getImgUrl());
			}

		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			ServiceLogger.printErrorLog("Exception occured in" + "getCitiBankOffers", e);
			try {
				mailManager.sendMail(e, "getCitiBankOffers");
			} catch (IOException e1) {
				ServiceLogger.printErrorLog("Exception occured while calling sentMail()", e1);
			}
		} finally {
			if(session!=null)
				session.close();
		}
		ServiceLogger.printInfoLog("Return value " + offerList.toString());

		return offerList;
	}

	@Override
	public String getSystemParam(String sysParam) {
		ServiceLogger.printInfoLog("Entered getSystemParam");
		Session session = null;
		Transaction transaction = null;
		String value = null;
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			value = (String) session.createCriteria(SystemParam.class).add(Restrictions.eq("sysParam", sysParam))
					.setProjection(Projections.property("value")).uniqueResult();
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			ServiceLogger.printErrorLog("Exception occured in" + "getSystemParam", e);
			try {
				mailManager.sendMail(e, "getSystemParam");
			} catch (IOException e1) {
				ServiceLogger.printErrorLog("Exception occured while calling sentMail()", e1);
			}
		} finally {
			if(session!=null)
				session.close();
		}
		ServiceLogger.printInfoLog("Return value " + value);

		return value;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CitiBankOffers> getOffer(LocationDetails locationDetails) {
		ServiceLogger.printInfoLog("Entered getOffer");
		Session session = null;
		Transaction transaction = null;
		List<CitiBankOffers> offerList = new ArrayList<>();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			offerList = session.createCriteria(CitiBankOffers.class)
					.add(Restrictions.like("actualCity", locationDetails.getCity(), MatchMode.ANYWHERE))
					.add(Restrictions.like("companyName", locationDetails.getMerchantName(), MatchMode.EXACT))
					.add(Restrictions.ne("longitude", 0.0)).add(Restrictions.ne("lattitude", 0.0)).list();

			if (offerList.isEmpty()) {
				offerList = session.createCriteria(CitiBankOffers.class)
						.add(Restrictions.like("companyName", locationDetails.getMerchantName(), MatchMode.EXACT))
						.add(Restrictions.ne("longitude", 0.0)).add(Restrictions.ne("lattitude", 0.0)).list();
			}
		
			transaction.commit();
			for (CitiBankOffers offers : offerList) {
				offers.setImgUrl(CommonDomain.returnBaseUrl() + offers.getImgUrl());
			}

		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			ServiceLogger.printErrorLog("Exception occured in" + "getOffer", e);
			try {
				mailManager.sendMail(e, "getOffer");
			} catch (IOException e1) {
				ServiceLogger.printErrorLog("Exception occured while calling sentMail()", e1);
			}
		} finally {
			if(session!=null)
				session.close();
		}
		
		return offerList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CitiBankOffers> getCitiBankCardOffers(LocationDetails locationDetails, Coordinates coordinates) {
		ServiceLogger.printInfoLog("Entered getCitiBankCardOffers");
		Session session = null;
		Transaction transaction = null;
		List<CitiBankOffers> returnOfferList = new ArrayList<>();
		List<CitiBankOffers> offerList = new ArrayList<>();
		List<OfferCardMapping> offerCardMapping = new ArrayList<>();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			if (locationDetails.getCoordinates() != null && !locationDetails.getCoordinates().equalsIgnoreCase("")) {
				offerList = session.createCriteria(CitiBankOffers.class)
						.add(Restrictions.eq("offerCategory", locationDetails.getCategory()))
						.add(Restrictions.eq("actualCity", locationDetails.getActualCity()))
						.addOrder(Order.desc("offerDate")).list();

				if (offerList.isEmpty()) {
					offerList = session.createCriteria(CitiBankOffers.class)
							.add(Restrictions.eq("offerCategory", locationDetails.getCategory()))
							.addOrder(Order.desc("offerDate")).list();
				}
			} else {
				offerList = session.createCriteria(CitiBankOffers.class)
						.add(Restrictions.eq("offerCategory", locationDetails.getCategory()))
						.add(Restrictions.eq("longitude", 0.0)).add(Restrictions.eq("lattitude", 0.0))
						.add(Restrictions.eq("actualCity", locationDetails.getActualCity()))
						.addOrder(Order.desc("offerDate")).setMaxResults(3).list();

				if (offerList.isEmpty()) {
					offerList = session.createCriteria(CitiBankOffers.class)
							.add(Restrictions.eq("offerCategory", locationDetails.getCategory()))
							.add(Restrictions.eq("longitude", 0.0)).add(Restrictions.eq("lattitude", 0.0))
							.addOrder(Order.desc("offerDate")).setMaxResults(3).list();
				}
			}
			offerCardMapping = session.createCriteria(OfferCardMapping.class)
					.add(Restrictions.eq("cardId", locationDetails.getCardId())).list();
			for (int i = 0; i < offerCardMapping.size(); i++) {
				for (int j = 0; j < offerList.size(); j++) {
					if (offerList.get(j).getOfferId().equals(offerCardMapping.get(i).getOfferId())) {
						CitiBankOffers reurnOffer = offerList.get(j);
						returnOfferList.add(reurnOffer);
					}
				}
			}
			transaction.commit();
			for (CitiBankOffers offers : returnOfferList) {
				offers.setImgUrl(CommonDomain.returnBaseUrl() + offers.getImgUrl());
			}

		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			ServiceLogger.printErrorLog("Exception occured in" + "getCitiBankCardOffers", e);
			try {
				mailManager.sendMail(e, "getCitiBankCardOffers");
			} catch (IOException e1) {
				ServiceLogger.printErrorLog("Exception occured while calling sentMail()", e1);
			}
		} finally {
			if(session!=null)
				session.close();
		}
		ServiceLogger.printInfoLog("Return value " + returnOfferList.toString());

		return returnOfferList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CitiBankOffers> getCitiBankAllCardOffers(LocationDetails locationDetails, Coordinates coordinates) {
		ServiceLogger.printInfoLog("Entered getCitiBankAllCardOffers");
		Session session = null;
		Transaction transaction = null;
		List<CitiBankOffers> offerList = new ArrayList<>();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			if (locationDetails.getCoordinates() != null && !locationDetails.getCoordinates().equalsIgnoreCase("")) {
				offerList = session.createCriteria(CitiBankOffers.class)
						.add(Restrictions.eq("actualCity", locationDetails.getActualCity()))
						.addOrder(Order.desc("offerDate")).list();

				if (offerList.isEmpty()) {
					offerList = session.createCriteria(CitiBankOffers.class).addOrder(Order.desc("offerDate")).list();

				}
			} else {

				offerList = session.createCriteria(CitiBankOffers.class).add(Restrictions.eq("longitude", 0.0))
						.add(Restrictions.eq("lattitude", 0.0))
						.add(Restrictions.eq("actualCity", locationDetails.getActualCity()))
						.addOrder(Order.desc("offerDate")).setMaxResults(3).list();

				if (offerList.isEmpty()) {
					offerList = session.createCriteria(CitiBankOffers.class).add(Restrictions.eq("longitude", 0.0))
							.add(Restrictions.eq("lattitude", 0.0)).addOrder(Order.desc("offerDate")).setMaxResults(3)
							.list();
				}
			}
		
			transaction.commit();
			for (CitiBankOffers offers : offerList) {
				offers.setImgUrl(CommonDomain.returnBaseUrl() + offers.getImgUrl());
			}

		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			ServiceLogger.printErrorLog("Exception occured in" + "getCitiBankAllCardOffers", e);
			try {
				mailManager.sendMail(e, "getCitiBankAllCardOffers");
			} catch (IOException e1) {
				ServiceLogger.printErrorLog("Exception occured while calling sentMail()", e1);
			}
		} finally {
			if(session!=null)
				session.close();
		}
		ServiceLogger.printInfoLog("Return value " + offerList.toString());

		return offerList;
	}

	@SuppressWarnings({ "unchecked", "null" })
	@Override
	public Map<String, String> getAPIKeys() {
		ServiceLogger.printInfoLog("Entered getAPIKeys");
		Session session = null;
		Transaction transaction = null;
		List<SysParam> apiParams = new ArrayList<>();
		Map<String, String> apiMap = new HashMap<>();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			apiParams = session.createCriteria(SysParam.class)
					.add(Restrictions.like("apiName", "citioffergoogleUrlApiKey" + "%")).list();
			for (SysParam sysParam : apiParams) {
				apiMap.put(sysParam.getApiName(), sysParam.getApiKey());
			}
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			ServiceLogger.printErrorLog("Exception occured in" + "getAPIKeys", e);
			try {
				mailManager.sendMail(e, "getAPIKeys");
			} catch (IOException e1) {
				ServiceLogger.printErrorLog("Exception occured while calling sentMail()", e1);
			}
		} finally {
			if(session!=null)
				session.close();
		}
		ServiceLogger.printInfoLog("Return value " + apiMap.toString());

		return apiMap;
	}

	@Override
	public List<CitiBankOffers> getAllOffers() {
		ServiceLogger.printInfoLog("Entered getAllOffers");
		Session session = null;
		List<CitiBankOffers> offerList = new ArrayList<>();
		try {
			session = sessionFactory.openSession();
			offerList = session.createCriteria(CitiBankOffers.class).list();
			if (offerList != null && !offerList.isEmpty()) {
				for (CitiBankOffers citiBankOffers : offerList) {
					citiBankOffers.setImgUrl(CommonDomain.returnBaseUrl() + citiBankOffers.getImgUrl());
				}
			}
		} catch (Exception e) {
			ServiceLogger.printErrorLog("Exception occured in" + "getAllOffers", e);
			try {
				mailManager.sendMail(e, "getAllOffers");
			} catch (IOException e1) {
				ServiceLogger.printErrorLog("Exception occured while calling sentMail()", e1);
			}
		} finally {
			if(session!=null)
				session.close();
		}
		
		return offerList;

	}

	@Override
	public List<OfferMapping> getUserDetails(UserInputFinal userInputFinal) {
		ServiceLogger.printInfoLog("Entered getUserDetails");
		Session session = null;
		Transaction transaction = null;
		List<OfferMapping> listOfferMapping = new ArrayList<>();
		Criterion conveyanceRest = null;
		Criterion weatherRest = null;
		Criterion dayRest = null;
		Criterion timeRest = null;
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

			if (userInputFinal.getConveyance() != null) {
				Criterion rest1 = Restrictions.eq("categoryType", ServiceConstants.Conveyance);
				Criterion rest2 = Restrictions.eq("categoryValue", userInputFinal.getConveyance());
				conveyanceRest = Restrictions.and(rest1, rest2);
			} else {
				Criterion rest1 = Restrictions.eq("categoryType", ServiceConstants.Conveyance);
				Criterion rest2 = Restrictions.eq("categoryValue", "''");
				conveyanceRest = Restrictions.and(rest1, rest2);
			}
			if (userInputFinal.getWeather() != null && !userInputFinal.getWeather().isEmpty()) {
				Criterion rest3 = Restrictions.eq("categoryType", ServiceConstants.Weather);
				Criterion rest4 = Restrictions.in("categoryValue", userInputFinal.getWeather());
				weatherRest = Restrictions.and(rest3, rest4);
			} else {
				Criterion rest3 = Restrictions.eq("categoryType", ServiceConstants.Weather);
				Criterion rest4 = Restrictions.eq("categoryValue", "''");
				weatherRest = Restrictions.and(rest3, rest4);
			}
			if (userInputFinal.getDay() != null) {
				Criterion rest5 = Restrictions.eq("categoryType", ServiceConstants.Days);
				Criterion rest6 = Restrictions.eq("categoryValue", userInputFinal.getDay());
				dayRest = Restrictions.and(rest5, rest6);
			} else {
				Criterion rest5 = Restrictions.eq("categoryType", ServiceConstants.Days);
				Criterion rest6 = Restrictions.eq("categoryValue", "''");
				dayRest = Restrictions.and(rest5, rest6);
			}
			if (userInputFinal.getDate() != null) {
				Date date = dateFormat.parse(userInputFinal.getDate());
				String sql = "select CAST('" + dateFormat.format(date) + "' AS TIME)";
				Time datetime = (Time) session.createSQLQuery(sql).uniqueResult();
				ServiceLogger.printDebugLog("dateTime:" + datetime);
				String startTime = (String) session.createCriteria(CategoryMapping.class)
						.add(Restrictions.eq("categoryType", ServiceConstants.StartTimeRange))
						.setProjection(Projections.property("categoryValue")).uniqueResult();
				String endTime = (String) session.createCriteria(CategoryMapping.class)
						.add(Restrictions.eq("categoryType", ServiceConstants.EndTimeRange))
						.setProjection(Projections.property("categoryValue")).uniqueResult();
				timeRest = Restrictions
						.sqlRestriction("'" + datetime + "'" + "BETWEEN '" + startTime + "'" + "AND '" + endTime + "'");
			}
			DetachedCriteria subquery = DetachedCriteria.forClass(CategoryMapping.class)
					.setProjection(Projections.property("geoOfferId"))
					.add(Restrictions.disjunction().add(conveyanceRest).add(weatherRest).add(dayRest).add(timeRest));
			Criteria criteria = session.createCriteria(OfferMapping.class, "offers")
					.add(Subqueries.propertyIn("offers.geoOfferId", subquery));
			listOfferMapping = criteria.list();
			ServiceLogger.printInfoLog("OfferList inside getUserDetails()" + listOfferMapping.toString());
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			ServiceLogger.printErrorLog("Exception occured in" + "getUserDetails", e);
			try {
				mailManager.sendMail(e, "getUserDetails");
			} catch (IOException e1) {
				ServiceLogger.printErrorLog("Exception occured while calling sentMail()", e1);
			}
		} finally {
			if(session!=null)
				session.close();
		}
		ServiceLogger.printInfoLog("Return value " + listOfferMapping.toString());

		return listOfferMapping;

	}

	@Override
	public UserOutputFinal getOfferIdByGeoCoordinate(UserOutputFinal userOutputFinal) {
		ServiceLogger.printInfoLog("Entered getOfferIdByGeoCoordinate");
		Session session = null;
		Transaction transaction = null;
		List<CitiBankOffers> listCitiBankOffers = new ArrayList<>();
		List<Integer> listOfferId = new ArrayList<>();
		OfferMapping offerMapping = new OfferMapping();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			Query query = session
					.createQuery("Select om.geoOfferId FROM OfferMapping om where om.geoOfferId = :geoOfferId")
					.setInteger("geoOfferId", userOutputFinal.getGeoOfferId());
			@SuppressWarnings("unchecked")
			Integer geoOfferId = (Integer) query.uniqueResult();

			offerMapping.setGeoOfferId(geoOfferId);
			userOutputFinal.setGeoOfferId(offerMapping.getGeoOfferId());
			listOfferId = session.createCriteria(GeoOfferMapping.class)
					.add(Restrictions.eq("geoOfferId", userOutputFinal.getGeoOfferId()))
					.setProjection(Projections.property("offerId")).list();
			for (Integer geoOffer : listOfferId) {
				CitiBankOffers offer = (CitiBankOffers) session.createCriteria(CitiBankOffers.class)
						.add(Restrictions.eq("offerId", geoOffer)).uniqueResult();
				if (offer != null)
					listCitiBankOffers.add(offer);

			}
			transaction.commit();

			for (CitiBankOffers offers : listCitiBankOffers) {

				offers.setImgUrl(CommonDomain.returnBaseUrl() + offers.getImgUrl());
			}
			userOutputFinal.setListCitiBankOffers(listCitiBankOffers);
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			ServiceLogger.printErrorLog("Exception occured in" + "getOfferIdByGeoCoordinate", e);
			try {
				mailManager.sendMail(e, "getOfferIdByGeoCoordinate");
			} catch (IOException e1) {
				ServiceLogger.printErrorLog("Exception occured while calling sentMail()", e1);
			}
		} finally {
			if(session!=null)
				session.close();
		}
		ServiceLogger.printInfoLog("Return value " + userOutputFinal.toString());

		return userOutputFinal;

	}

	// added for merchant offers
	@Override
	public List<CitiBankOffers> getMerchantOffers(Integer merchantId) {
		ServiceLogger.printInfoLog("Entered getMerchantOffers");
		Session session = null;
		Transaction transaction = null;
		List<CitiBankOffers> merchantOfferList = new ArrayList<>();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			DetachedCriteria subquery = DetachedCriteria.forClass(OfferMerchantMapping.class, "merchant")
					.setProjection(Projections.distinct(Projections.property("merchant.offerId")));

			Criteria criteria = session.createCriteria(CitiBankOffers.class, "offers")
					.add(Subqueries.propertyIn("offers.offerId", subquery));

			merchantOfferList = criteria.list();
			transaction.commit();
			for (CitiBankOffers offers : merchantOfferList) {
				offers.setImgUrl(CommonDomain.returnBaseUrl() + offers.getImgUrl());
			}
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			ServiceLogger.printErrorLog("Exception occured in" + "getMerchantOffers", e);
			try {
				mailManager.sendMail(e, "getMerchantOffers");
			} catch (IOException e1) {
				ServiceLogger.printErrorLog("Exception occured while calling sentMail()", e1);
			}
		} finally {
			if(session!=null)
				session.close();
		}
		ServiceLogger.printInfoLog("Return value " + merchantOfferList.toString());

		return merchantOfferList;
	}

	@Override
	public LoginDetails authenticateLogin(LoginDetails logindetails) {
		ServiceLogger.printInfoLog("Entered authenticateLogin");
		Session session = null;
		Transaction transaction = null;
		LoginDetails returnDetails = new LoginDetails();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			MerchantDetails merchantDetails = (MerchantDetails) session.createCriteria(MerchantDetails.class)
					.add(Restrictions.eq(ServiceConstants.UserPinColumn, logindetails.getUserPin())).uniqueResult();
			if (merchantDetails != null) {
				if (merchantDetails.getMerchantType().equalsIgnoreCase(ServiceConstants.Bank)) {
					List<CitiBankOffers> offerList = session.createCriteria(CitiBankOffers.class).list();
					for (CitiBankOffers citiBankOffers : offerList) {
						citiBankOffers.setImgUrl(CommonDomain.returnBaseUrl() + citiBankOffers.getImgUrl());
					}
					returnDetails.setMerchantId(merchantDetails.getMerchantId());
					returnDetails.setMerchantName(merchantDetails.getMerchantName());
					returnDetails.setOfferList(offerList);
					returnDetails.setImgUrl(CommonDomain.returnBaseUrl() + merchantDetails.getImgUrl());
					returnDetails.setStatus(true);
				} else if (merchantDetails.getMerchantType().equalsIgnoreCase(ServiceConstants.Merchant)) {
					List<CitiBankOffers> offerList = session.createCriteria(CitiBankOffers.class)
							.add(Restrictions.eq(ServiceConstants.MerchantIDColumn, merchantDetails.getMerchantId()))
							.list();
					for (CitiBankOffers citiBankOffers : offerList) {
						citiBankOffers.setImgUrl(CommonDomain.returnBaseUrl() + citiBankOffers.getImgUrl());
					}
					returnDetails.setMerchantId(merchantDetails.getMerchantId());
					returnDetails.setMerchantName(merchantDetails.getMerchantName());
					returnDetails.setOfferList(offerList);
					returnDetails.setImgUrl(CommonDomain.returnBaseUrl() + merchantDetails.getImgUrl());
					returnDetails.setStatus(true);
				}
			} else {
				returnDetails.setMessage("Invalid User Pin");
				returnDetails.setStatus(false);
			}

		} catch (Exception e) {
			returnDetails.setMessage("Exception occured while authenticating user");
			returnDetails.setStatus(false);
			if (transaction != null)
				transaction.rollback();
			ServiceLogger.printErrorLog("Exception occured in" + "authenticateLogin", e);
			try {
				mailManager.sendMail(e, "authenticateLogin");
			} catch (IOException e1) {
				ServiceLogger.printErrorLog("Exception occured while calling sentMail()", e1);
			}
		} finally {
			if(session!=null)
				session.close();
		}
		ServiceLogger.printInfoLog("Return value " + returnDetails.toString());

		return returnDetails;
	}

	/*
	 * @SuppressWarnings("unchecked")
	 *
	 * @Override public ResponseMessage insertOffers(GeoOfferDetails
	 * geoOfferdetails) { SimpleDateFormat displayFormat = new
	 * SimpleDateFormat("HH:mm:ss"); SimpleDateFormat parseFormat = new
	 * SimpleDateFormat("hh:mm a"); Session session = null ; Transaction
	 * transaction = null; GeoOfferMapping geoOfferMapping = null; OfferMapping
	 * offerMapping = new OfferMapping(); ResponseMessage response = new
	 * ResponseMessage(); try{ session = sessionFactory.openSession();
	 * transaction = session.beginTransaction();
	 *
	 * // if same geo coordinate and radius exist delete the previous records
	 * and then insert List<OfferMapping>
	 * offersList=session.createCriteria(OfferMapping.class)
	 * .add(Restrictions.eq("geoCoordinate",
	 * geoOfferdetails.getGeoCoordinates())) .add(Restrictions.eq("radius",
	 * geoOfferdetails.getRadius())).list(); if(!offersList.isEmpty()){ Query
	 * query = session.createQuery(
	 * "delete from OfferMapping where geoCoordinate = :geoCoordinate and radius = :radius"
	 * ); query.setParameter("geoCoordinate",
	 * geoOfferdetails.getGeoCoordinates()); query.setParameter("radius",
	 * geoOfferdetails.getRadius()); int result = query.executeUpdate();
	 * if(result>0){ ServiceLogger.printInfoLog("Geofence detials removed"); }
	 * for (OfferMapping offerMapping2 : offersList) { Query geoquery =
	 * session.createQuery
	 * ("delete from GeoOfferMapping where geoOfferId = :geoOfferId");
	 * geoquery.setParameter("geoOfferId", offerMapping2.getGeoOfferId()); int
	 * geoResult=geoquery.executeUpdate(); if(geoResult>0){
	 * ServiceLogger.printInfoLog("Offer detials removed"); } } }
	 *
	 * Integer maxGeoOfferId=(Integer)
	 * session.createCriteria(GeoOfferMapping.class
	 * ).setProjection(Projections.max
	 * (ServiceConstants.GeoOfferIDColumn)).uniqueResult();
	 *
	 * if(maxGeoOfferId==null) maxGeoOfferId= 0; else maxGeoOfferId=
	 * maxGeoOfferId+1; for(int i=0;i<geoOfferdetails.getList().size();i++){
	 * geoOfferMapping = new GeoOfferMapping();
	 * geoOfferMapping.setOfferId(geoOfferdetails
	 * .getList().get(i).getOfferId());
	 * geoOfferMapping.setGeoOfferId(maxGeoOfferId);
	 * session.save(geoOfferMapping); }
	 * offerMapping.setGeoCoordinate(geoOfferdetails.getGeoCoordinates());
	 * offerMapping.setRadius(geoOfferdetails.getRadius());
	 * offerMapping.setGeoOfferId(maxGeoOfferId);
	 * offerMapping.setDay(geoOfferdetails.getDay());
	 * offerMapping.setConveyance(geoOfferdetails.getConveyance());
	 * offerMapping.setWeather(geoOfferdetails.getWeather()); String[]
	 * splittedTimeRange=geoOfferdetails.getTimeRange().split("-"); Date
	 * startRange = parseFormat.parse(splittedTimeRange[0]);
	 * offerMapping.setStartTimeRange(displayFormat.format(startRange)); Date
	 * endRange = parseFormat.parse(splittedTimeRange[1]);
	 * offerMapping.setEndTimeRange(displayFormat.format(endRange));
	 * session.save(offerMapping);
	 * response.setMessage("Geo Fence detials succesfully stored in DB");
	 * response.setStatus(true); transaction.commit(); }catch(Exception e){
	 * e.printStackTrace();
	 * response.setMessage("Exception occured while inserting data into DB");
	 * response.setStatus(false); transaction.rollback(); }finally{
	 * session.close(); } return response; }
	 */

	@SuppressWarnings("unchecked")
	@Override
	public ResponseMessage insertOffers(GeoOfferDetails geoOfferdetails) {
		ServiceLogger.printInfoLog("Entered insertOffers");
		SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm:ss");
		SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
		Session session = null;
		Transaction transaction = null;
		GeoOfferMapping geoOfferMapping = null;
		CategoryMapping categoryMapping = null;
		List<CategoryMapping> listCategoryMapping = new ArrayList<>();
		OfferMapping offerMapping = new OfferMapping();
		ResponseMessage response = new ResponseMessage();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();

			// if same geo coordinate and radius exist delete the previous
			// records and then insert
			List<OfferMapping> offersList = session.createCriteria(OfferMapping.class).list();
			if (!offersList.isEmpty()) {
				Query query = session.createQuery("delete from OfferMapping");
				int result = query.executeUpdate();
				if (result > 0) {
					ServiceLogger.printInfoLog("Geofence detials removed");
				}
			}
			List<GeoOfferMapping> GeooffersList = session.createCriteria(GeoOfferMapping.class).list();
			if (!GeooffersList.isEmpty()) {
				Query query = session.createQuery("delete from GeoOfferMapping");
				int result = query.executeUpdate();
				if (result > 0) {
					ServiceLogger.printInfoLog("Offer detials removed");
				}

			}
			List<CategoryMapping> categoryMappingList = session.createCriteria(CategoryMapping.class).list();
			if (!categoryMappingList.isEmpty()) {
				Query query = session.createQuery("delete from CategoryMapping");
				int result = query.executeUpdate();
				if (result > 0) {
					ServiceLogger.printInfoLog("Category Mapping detials removed");
				}
			}

			Integer maxGeoOfferId = (Integer) session.createCriteria(GeoOfferMapping.class)
					.setProjection(Projections.max(ServiceConstants.GeoOfferIDColumn)).uniqueResult();
			if (maxGeoOfferId == null)
				maxGeoOfferId = 1;
			else
				maxGeoOfferId = maxGeoOfferId + 1;

			// save Offers
			for (int i = 0; i < geoOfferdetails.getList().size(); i++) {
				geoOfferMapping = new GeoOfferMapping();
				geoOfferMapping.setOfferId(geoOfferdetails.getList().get(i).getOfferId());
				geoOfferMapping.setGeoOfferId(maxGeoOfferId);
				session.save(geoOfferMapping);
			}
			// save Conveyance
			List<String> conveyanceList = geoOfferdetails.getConveyanceList();
			if (!conveyanceList.isEmpty() && conveyanceList != null) {
				for (String conveyance : conveyanceList) {
					categoryMapping = new CategoryMapping();
					categoryMapping.setGeoOfferId(maxGeoOfferId);
					categoryMapping.setCategoryType(ServiceConstants.Conveyance);
					categoryMapping.setCategoryValue(conveyance);
					listCategoryMapping.add(categoryMapping);
				}
			}
			// save Weather
			List<String> weatherList = geoOfferdetails.getWeatherList();
			if (weatherList != null) {
				for (String weather : weatherList) {
					categoryMapping = new CategoryMapping();
					categoryMapping.setGeoOfferId(maxGeoOfferId);
					categoryMapping.setCategoryType(ServiceConstants.Weather);
					categoryMapping.setCategoryValue(weather);
					listCategoryMapping.add(categoryMapping);
				}
			}
			// save days
			if (geoOfferdetails.getDay() != null) {
				CategoryMapping dayCategoryMapping = new CategoryMapping(maxGeoOfferId, ServiceConstants.Days,
						geoOfferdetails.getDay());
				listCategoryMapping.add(dayCategoryMapping);
			}
			if (geoOfferdetails.getTimeRange() != null) {
				String[] splittedTimeRange = geoOfferdetails.getTimeRange().split("-");
				Date startRange = parseFormat.parse(splittedTimeRange[0]);
				Date endRange = parseFormat.parse(splittedTimeRange[1]);
				// save startTime
				CategoryMapping startCategoryMapping = new CategoryMapping(maxGeoOfferId,
						ServiceConstants.StartTimeRange, displayFormat.format(startRange));
				listCategoryMapping.add(startCategoryMapping);
				// save endTime
				CategoryMapping endCategoryMapping = new CategoryMapping(maxGeoOfferId, ServiceConstants.EndTimeRange,
						displayFormat.format(endRange));
				listCategoryMapping.add(endCategoryMapping);
			}
			// save categories
			for (CategoryMapping listCategory : listCategoryMapping) {
				session.save(listCategory);
			}
			offerMapping.setGeoCoordinate(geoOfferdetails.getGeoCoordinates());
			offerMapping.setRadius(geoOfferdetails.getRadius());
			offerMapping.setGeoOfferId(maxGeoOfferId);
			if (geoOfferdetails.getPolygonValues() != null) {
				String prefix = "";
				StringBuffer sf = new StringBuffer();
				for (String polygon : geoOfferdetails.getPolygonValues()) {
					sf.append(prefix);
					prefix = "|";
					sf.append(polygon);
				}
				offerMapping.setPolygonCoordinate(sf.toString());
			}
			session.save(offerMapping);

			response.setMessage("Geo Fence detials succesfully stored in DB");
			response.setStatus(true);
			transaction.commit();
		} catch (Exception e) {
			response.setMessage("Exception occured while inserting data into DB");
			response.setStatus(false);
			if (transaction != null)
				transaction.rollback();
			ServiceLogger.printErrorLog("Exception occured in" + "insertOffers", e);
			try {
				mailManager.sendMail(e, "insertOffers");
			} catch (IOException e1) {
				ServiceLogger.printErrorLog("Exception occured while calling sentMail()", e1);
			}
		} finally {
			if(session!=null)
				session.close();
		}
		ServiceLogger.printInfoLog("Return value " + response.toString());

		return response;
	}

	@Override
	public List<CitiBankOffers> loadOffers(LoginDetails logindetails) {
		ServiceLogger.printInfoLog("Entered loadOffers");
		Session session = null;
		Transaction transaction = null;
		List<CitiBankOffers> listOfferMapping = new ArrayList<>();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			DetachedCriteria subquery = DetachedCriteria.forClass(ResourceOfferMapping.class, "t")
					.setProjection(Projections.property("t.offerId"))
					.add(Restrictions.eq(ServiceConstants.CustomerIdColumn, logindetails.getCustomerId()));

			Criteria criteria = session.createCriteria(CitiBankOffers.class, "offers")
					.add(Subqueries.propertyIn("offers.offerId", subquery));

			listOfferMapping = criteria.list();
			transaction.commit();
			if (!listOfferMapping.isEmpty()) {
				for (CitiBankOffers citiBankOffers : listOfferMapping) {
					citiBankOffers.setImgUrl(CommonDomain.returnBaseUrl() + citiBankOffers.getImgUrl());
				}
			} else {
				CitiBankOffers citiBankOffers = new CitiBankOffers();
				listOfferMapping.add(citiBankOffers);
			}

		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			ServiceLogger.printErrorLog("Exception occured in" + "loadOffers", e);
			try {
				mailManager.sendMail(e, "loadOffers");
			} catch (IOException e1) {
				ServiceLogger.printErrorLog("Exception occured while calling sentMail()", e1);
			}
		} finally {
			if(session!=null)
				session.close();
		}
		ServiceLogger.printInfoLog("Return value " + listOfferMapping.toString());

		return listOfferMapping;
	}

}
