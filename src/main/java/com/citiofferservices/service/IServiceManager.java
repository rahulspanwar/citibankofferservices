package com.citiofferservices.service;

import java.util.List;
import java.util.Map;

import com.citiofferservices.data.CitiBankOffers;
import com.citiofferservices.data.GeoOfferDetails;
import com.citiofferservices.data.LocationDetails;
import com.citiofferservices.data.LoginDetails;
import com.citiofferservices.data.ResponseMessage;
import com.citiofferservices.data.UserInputFinal;
import com.citiofferservices.data.UserOutputFinal;

public interface IServiceManager {

	List<CitiBankOffers> getCitiBankOffer(LocationDetails locationDetails);

	LocationDetails getUserAddress(String location);

	CitiBankOffers getOffer(LocationDetails locationDetails);

	List<CitiBankOffers> getCardOffers(LocationDetails locationDetails);

	List<CitiBankOffers> getAllOffers(LocationDetails locationDetails);

	UserOutputFinal getUserDetailsByDistance(UserInputFinal userInputFinal);

	List<CitiBankOffers> getMerchantOffers(Integer merchantId);

	LoginDetails login(LoginDetails logindetails);

	ResponseMessage insertOffers(GeoOfferDetails geoOfferdetails);

	List<CitiBankOffers> loadOffers(LoginDetails logindetails);

	public List<String> getAPIKeys();

}
