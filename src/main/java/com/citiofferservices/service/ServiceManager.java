package com.citiofferservices.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citiofferservices.dao.IServiceDao;
import com.citiofferservices.dao.ServiceDao;
import com.citiofferservices.data.CitiBankOffers;
import com.citiofferservices.data.Coordinates;
import com.citiofferservices.data.GeoOfferDetails;
import com.citiofferservices.data.LocationDetails;
import com.citiofferservices.data.LoginDetails;
import com.citiofferservices.data.OfferMapping;
import com.citiofferservices.data.ResponseMessage;
import com.citiofferservices.data.UserInputFinal;
import com.citiofferservices.data.UserOutputFinal;
import com.citiofferservices.utils.GeofenceClass;
import com.citiofferservices.utils.HelperClass;
import com.citiofferservices.utils.ServiceLogger;

@Service
public class ServiceManager implements IServiceManager {

	@Autowired
	private IServiceDao serviceDao;

	@Autowired
	private HelperClass helperClass;

	@Override
	public List<CitiBankOffers> getCitiBankOffer(LocationDetails locationDetails) {
		ServiceLogger.printInfoLog("Entered getCitiBankOffer");
		Coordinates coordinates = null;

		String alphaRegex = ".*[a-zA-Z].*";
		List<CitiBankOffers> outputList = new ArrayList<>();
		if (!locationDetails.getCoordinates().matches(alphaRegex)) {

			if (locationDetails.getCoordinates() != null && !locationDetails.getCoordinates().equalsIgnoreCase(""))
				coordinates = HelperClass.coordinatesSeparation(locationDetails.getCoordinates());
			List<CitiBankOffers> citiBankOffersList = serviceDao.getCitiBankOffers(locationDetails, coordinates);
			String maxDistance = serviceDao.getSystemParam("km_range");

			List<CitiBankOffers> opwithoutcordinates = new ArrayList<>();
			List<CitiBankOffers> opwithcordinates = new ArrayList<>();
			Map<String, String> apiMap = new ServiceDao().getAPIKeys();
			List<String> apiList = helperClass.getApiKey(apiMap);

			if (locationDetails.getCoordinates() != null && !locationDetails.getCoordinates().equalsIgnoreCase("")) {
				for (CitiBankOffers citiBankOffers : citiBankOffersList) {
					if (citiBankOffers.getLattitude() != null && citiBankOffers.getLongitude() != null) {

						coordinates.setDesLatitude(citiBankOffers.getLattitude());
						coordinates.setDesLongitude(citiBankOffers.getLongitude());
						if (citiBankOffers.getAddress() == null || citiBankOffers.getAddress() == "")
							citiBankOffers.setAddress("ADDRESS_UNAVAILABLE");

						Coordinates cordinates = new Coordinates();

						cordinates = helperClass.distance(coordinates, maxDistance);

						if (!cordinates.getDistance().equals("-1")) {
							citiBankOffers.setDistance(cordinates.getDistance());
							opwithcordinates.add(citiBankOffers);
						}
					} /*
						 * else { opwithoutcordinates.add(citiBankOffers); }
						 */

				}
				Collections.sort(opwithcordinates, new Comparator<CitiBankOffers>() {

					@Override
					public int compare(CitiBankOffers o1, CitiBankOffers o2) {
						Double distance1 = Double.valueOf(o1.getDistance().replaceAll("[^0-9.]", ""));
						Double distance2 = Double.valueOf(o2.getDistance().replaceAll("[^0-9.]", ""));
						return distance1.compareTo(distance2);
					}
				});
				appendOutput(outputList, opwithcordinates);
				appendOutput(outputList, opwithoutcordinates);
			} else {
				for (CitiBankOffers citiBankOffers : citiBankOffersList) {
					outputList.add(citiBankOffers);
				}
			}

			for (CitiBankOffers citiBankOffers : outputList) {
				coordinates.setDesLatitude(citiBankOffers.getLattitude());
				coordinates.setDesLongitude(citiBankOffers.getLongitude());
				for (String api : apiList) {
					coordinates = helperClass.apiCallForDistance(coordinates, api);
					if (coordinates.getDistance() != null) {
						break;
					} else {
						coordinates = helperClass.apiCallForDistance(coordinates, api);
					}
				}

				citiBankOffers.setDistance(coordinates.getDistance());
				citiBankOffers.setResponseStatus(coordinates.getResponseStatus());
			}

			if (outputList.isEmpty()) {
				CitiBankOffers bankOffers = new CitiBankOffers();
				bankOffers.setResponseStatus("NO_DATA_FOUND");
				outputList.add(bankOffers);
			}
		} else {
			CitiBankOffers bankOffers = new CitiBankOffers();
			bankOffers.setResponseStatus("INVALID_COORDINATES");
			outputList.add(bankOffers);
		}
		return outputList;
	}

	private void appendOutput(List<CitiBankOffers> outputList, List<CitiBankOffers> opwithoutcordinates) {
		for (CitiBankOffers citiBankOffers : opwithoutcordinates) {
			if (outputList.size() < 3) {
				outputList.add(citiBankOffers);
			}
		}

	}

	@Override
	public LocationDetails getUserAddress(String location) {
		ServiceLogger.printInfoLog("Entered getUserAddress");

		Coordinates coordinates = HelperClass.coordinatesSeparation(location);
		Map<String, String> apiMap = serviceDao.getAPIKeys();
		LocationDetails loactionDetails = helperClass.apiCallForAddress(location, apiMap);
		if (coordinates.getSrcLatitude() !=null && coordinates.getSrcLongitude() !=null && loactionDetails != null)
			loactionDetails.setCoordinates(location);
		return loactionDetails;
	}

	@Override
	public CitiBankOffers getOffer(LocationDetails locationDetails) {
		ServiceLogger.printInfoLog("Entered getOffer");
		List<CitiBankOffers> offerList = serviceDao.getOffer(locationDetails);
		List<CitiBankOffers> offerDataList = new ArrayList<>();
		CitiBankOffers bankOffers = null;
		List<CitiBankOffers> outputList = new ArrayList<>();
		CitiBankOffers response = new CitiBankOffers();
		String alphaRegex = ".*[a-zA-Z].*";
		if (!locationDetails.getCoordinates().matches(alphaRegex) && locationDetails.getCoordinates() != null
				&& locationDetails.getCoordinates() != "") {

			Coordinates coordinates = HelperClass.coordinatesSeparation(locationDetails.getCoordinates());

			String maxDistance = serviceDao.getSystemParam("km_range");
			Map<String, String> apiMap = new ServiceDao().getAPIKeys();
			List<String> apiList = helperClass.getApiKey(apiMap);

			for (CitiBankOffers offerData : offerList) {

				if (locationDetails.getCoordinates() != null
						&& !locationDetails.getCoordinates().equalsIgnoreCase("")) {
					if (offerData.getLattitude() != null && offerData.getLongitude() != null) {
						coordinates.setDesLatitude(offerData.getLattitude());
						coordinates.setDesLongitude(offerData.getLongitude());

						if (offerData.getAddress() == null || offerData.getAddress() == "")
							offerData.setAddress("ADDRESS_UNAVAILABLE");

						Coordinates cordinates = new Coordinates();

						cordinates = helperClass.distance(coordinates, maxDistance);

						if (!cordinates.getDistance().equals("-1"))
							offerData.setDistance(cordinates.getDistance());
						// offerData.setResponseStatus(cordinates.getResponseStatus());
					}
				}
			}

			for (CitiBankOffers citiBankOffers : offerList) {
				if (citiBankOffers.getDistance() != null) {
					bankOffers = new CitiBankOffers();
					bankOffers = citiBankOffers;
					offerDataList.add(bankOffers);
				}
			}
			for (CitiBankOffers citiBankOffers : offerDataList) {
				if ("-1".equalsIgnoreCase(citiBankOffers.getDistance())) {

					offerDataList.remove(citiBankOffers);
				}
			}
			if (offerDataList.size() > 1) {

				Collections.sort(offerDataList, new Comparator<CitiBankOffers>() {

					@Override
					public int compare(CitiBankOffers o1, CitiBankOffers o2) {
						Double distance1 = Double.valueOf(o1.getDistance().replaceAll("[^0-9.]", ""));
						Double distance2 = Double.valueOf(o2.getDistance().replaceAll("[^0-9.]", ""));
						return distance1.compareTo(distance2);
					}
				});

				for (CitiBankOffers citiBankOffers6 : offerDataList) {

					outputList.add(citiBankOffers6);
				}

				coordinates.setDesLatitude(outputList.get(0).getLattitude());
				coordinates.setDesLongitude(outputList.get(0).getLongitude());
				for (String api : apiList) {
					coordinates = helperClass.apiCallForDistance(coordinates, api);
					if (coordinates.getDistance() != null) {
						break;
					} else {
						coordinates = helperClass.apiCallForDistance(coordinates, api);
					}
				}

				outputList.get(0).setDistance(coordinates.getDistance());
				outputList.get(0).setResponseStatus(coordinates.getResponseStatus());

				response = outputList.get(0);
			} else if (offerDataList.size() == 1) {

				coordinates.setDesLatitude(offerDataList.get(0).getLattitude());
				coordinates.setDesLongitude(offerDataList.get(0).getLongitude());
				for (String api : apiList) {
					coordinates = helperClass.apiCallForDistance(coordinates, api);
					if (coordinates.getDistance() != null) {
						break;
					} else {
						coordinates = helperClass.apiCallForDistance(coordinates, api);
					}
				}

				offerDataList.get(0).setDistance(coordinates.getDistance());
				offerDataList.get(0).setResponseStatus(coordinates.getResponseStatus());

				response = offerDataList.get(0);

			} else {
				response = new CitiBankOffers();
				response.setResponseStatus("NO_DATA_FOUND");
			}
		} else {
			CitiBankOffers bankOffers3 = new CitiBankOffers();
			bankOffers3.setResponseStatus("INVALID_COORDINATES");
			response = bankOffers3;

		}

		return response;

	}

	@Override
	public List<CitiBankOffers> getCardOffers(LocationDetails locationDetails) {
		ServiceLogger.printInfoLog("Entered getCardOffers");
		String alphaRegex = ".*[a-zA-Z].*";
		List<CitiBankOffers> outputList = new ArrayList<>();
		if (!locationDetails.getCoordinates().matches(alphaRegex) && locationDetails.getCoordinates() != null
				&& locationDetails.getCoordinates() != "") {

			Coordinates coordinates = HelperClass.coordinatesSeparation(locationDetails.getCoordinates());
			List<CitiBankOffers> citiBankOffersList = serviceDao.getCitiBankCardOffers(locationDetails, coordinates);
			String maxDistance = serviceDao.getSystemParam("km_range");

			List<CitiBankOffers> opwithoutcordinates = new ArrayList<>();
			List<CitiBankOffers> opwithcordinates = new ArrayList<>();
			Map<String, String> apiMap = new ServiceDao().getAPIKeys();
			List<String> apiList = helperClass.getApiKey(apiMap);
			if (locationDetails.getCoordinates() != null && !locationDetails.getCoordinates().equalsIgnoreCase("")) {
				for (CitiBankOffers citiBankOffers : citiBankOffersList) {
					if (citiBankOffers.getLattitude() != null && citiBankOffers.getLongitude() != null) {
						coordinates.setDesLatitude(citiBankOffers.getLattitude());
						coordinates.setDesLongitude(citiBankOffers.getLongitude());

						if (citiBankOffers.getAddress() == null || citiBankOffers.getAddress() == "")
							citiBankOffers.setAddress("ADDRESS_UNAVAILABLE");

						Coordinates cordinates = new Coordinates();

						cordinates = helperClass.distance(coordinates, maxDistance);

						if (!"-1".equalsIgnoreCase(cordinates.getDistance())) {
							citiBankOffers.setDistance(cordinates.getDistance());
							// citiBankOffers.setResponseStatus(cordinates.getResponseStatus());
							opwithcordinates.add(citiBankOffers);
						}
					} /*
						 * else { opwithoutcordinates.add(citiBankOffers); }
						 */

				}
				Collections.sort(opwithcordinates, new Comparator<CitiBankOffers>() {

					@Override
					public int compare(CitiBankOffers o1, CitiBankOffers o2) {
						Double distance1 = Double.valueOf(o1.getDistance().replaceAll("[^0-9.]", ""));
						Double distance2 = Double.valueOf(o2.getDistance().replaceAll("[^0-9.]", ""));
						return distance1.compareTo(distance2);
					}
				});
				appendOutput(outputList, opwithcordinates);
				// appendOutput(outputList, opwithoutcordinates);
			} else {
				for (CitiBankOffers citiBankOffers : citiBankOffersList) {
					outputList.add(citiBankOffers);
				}
			}

			for (CitiBankOffers citiBankOffers : outputList) {
				coordinates.setDesLatitude(citiBankOffers.getLattitude());
				coordinates.setDesLongitude(citiBankOffers.getLongitude());
				for (String api : apiList) {
					coordinates = helperClass.apiCallForDistance(coordinates, api);
					if (coordinates.getDistance() != null) {
						break;
					} else {
						coordinates = helperClass.apiCallForDistance(coordinates, api);
					}
				}

				citiBankOffers.setDistance(coordinates.getDistance());
				citiBankOffers.setResponseStatus(coordinates.getResponseStatus());
			}
			if (outputList.isEmpty()) {
				CitiBankOffers bankOffers = new CitiBankOffers();
				bankOffers.setResponseStatus("NO_DATA_FOUND");
				outputList.add(bankOffers);
			}
		} else {
			CitiBankOffers bankOffers3 = new CitiBankOffers();
			bankOffers3.setResponseStatus("INVALID_COORDINATES");
			outputList.add(bankOffers3);

		}
		return outputList;
	}

	@Override
	public List<CitiBankOffers> getAllOffers(LocationDetails locationDetails) {
		ServiceLogger.printInfoLog("Entered getAllOffers");
		// List<CitiBankOffers> returnOfferList = serviceDao.getAllOffers();
		// return returnOfferList;
		String alphaRegex = ".*[a-zA-Z].*";
		List<CitiBankOffers> outputList = new ArrayList<>();
		List<CitiBankOffers> opwithcordinates = new ArrayList<>();

		if (!locationDetails.getCoordinates().matches(alphaRegex) && locationDetails.getCoordinates() != null
				&& locationDetails.getCoordinates() != "") {

			Coordinates coordinates = HelperClass.coordinatesSeparation(locationDetails.getCoordinates());
			List<CitiBankOffers> citiBankOffersList = serviceDao.getCitiBankAllCardOffers(locationDetails, coordinates);
			String maxDistance = serviceDao.getSystemParam("km_range");
			Map<String, String> apiMap = new ServiceDao().getAPIKeys();
			List<String> apiList = helperClass.getApiKey(apiMap);
			if (locationDetails.getCoordinates() != null && !locationDetails.getCoordinates().equalsIgnoreCase("")) {
				for (CitiBankOffers citiBankOffers : citiBankOffersList) {
					if (citiBankOffers.getLattitude() != null && citiBankOffers.getLongitude() != null) {
						coordinates.setDesLatitude(citiBankOffers.getLattitude());
						coordinates.setDesLongitude(citiBankOffers.getLongitude());

						if (citiBankOffers.getAddress() == null || citiBankOffers.getAddress() == "")
							citiBankOffers.setAddress("ADDRESS_UNAVAILABLE");

						Coordinates cordinates = new Coordinates();

						cordinates = helperClass.distance(coordinates, maxDistance);

						if (!"-1".equalsIgnoreCase(cordinates.getDistance())) {
							;
							citiBankOffers.setDistance(
									String.valueOf(helperClass.round(Double.valueOf(cordinates.getDistance()), 2)));
							citiBankOffers.setResponseStatus("API_SUCCESS");

							// citiBankOffers.setResponseStatus(cordinates.getResponseStatus());
							opwithcordinates.add(citiBankOffers);
						}
					} /*
						 * else { opwithoutcordinates.add(citiBankOffers); }
						 */

				}
				Collections.sort(opwithcordinates, new Comparator<CitiBankOffers>() {

					@Override
					public int compare(CitiBankOffers o1, CitiBankOffers o2) {
						Double distance1 = Double.valueOf(o1.getDistance().replaceAll("[^0-9.]", ""));
						Double distance2 = Double.valueOf(o2.getDistance().replaceAll("[^0-9.]", ""));
						return distance1.compareTo(distance2);
					}
				});
				// appendOutput(outputList, opwithcordinates);
				// appendOutput(outputList, opwithoutcordinates);
			} else {

				for (CitiBankOffers citiBankOffers : citiBankOffersList) {
					outputList.add(citiBankOffers);
				}
				return outputList;
			}

			// for (CitiBankOffers citiBankOffers : opwithcordinates) {
			// coordinates.setDesLatitude(citiBankOffers.getLattitude());
			// coordinates.setDesLongitude(citiBankOffers.getLongitude());
			// for (String api : apiList) {
			// coordinates = helperClass.apiCallForDistance(coordinates, api);
			// if (coordinates.getDistance() != null) {
			// break;
			// } else {
			// coordinates = helperClass.apiCallForDistance(coordinates, api);
			// }
			// }
			//
			// citiBankOffers.setDistance(coordinates.getDistance());
			// citiBankOffers.setResponseStatus(coordinates.getResponseStatus());
			// }

			if (opwithcordinates.isEmpty()) {
				CitiBankOffers bankOffers = new CitiBankOffers();
				bankOffers.setResponseStatus("NO_DATA_FOUND");
				opwithcordinates.add(bankOffers);
			}
		} else {
			CitiBankOffers bankOffers3 = new CitiBankOffers();
			bankOffers3.setResponseStatus("INVALID_COORDINATES");
			opwithcordinates.add(bankOffers3);

		}
		return opwithcordinates;
	}

	@Override
	public UserOutputFinal getUserDetailsByDistance(UserInputFinal userInputFinal) {
		ServiceLogger.printInfoLog("Entered getUserDetailsByDistance");
		UserOutputFinal userOutputFinal;
		String alphaRegex = ".*[a-zA-Z].*";
		userOutputFinal = new UserOutputFinal();
		GeofenceClass geofenceClass = new GeofenceClass();
		if (!userInputFinal.getGeoCoordinate().xCordinate.matches(alphaRegex)
				&& !userInputFinal.getGeoCoordinate().yCordinate.matches(alphaRegex)
				&& userInputFinal.getGeoCoordinate().getxCordinate() != null
				&& userInputFinal.getGeoCoordinate().getyCordinate() != null) {
			double geoX = Double.parseDouble(userInputFinal.getGeoCoordinate().xCordinate);
			double geoY = Double.parseDouble(userInputFinal.getGeoCoordinate().yCordinate);
			List<OfferMapping> list = serviceDao.getUserDetails(userInputFinal);
			ServiceLogger.printInfoLog("OfferList:" + list.toString());
			if (!list.isEmpty()) {
				for (OfferMapping offerMapping : list) {
					if (offerMapping.getRadius() != null) {
						Double result = geofenceClass.getGeoFenceByDistance(geoX, geoY,
								offerMapping.getGeoCoordinate());
						ServiceLogger.printDebugLog("distance:" + result + "radius:" + offerMapping.getRadius() / 1000);
						if (result <= offerMapping.getRadius() / 1000) {
							ServiceLogger.printDebugLog("in geoFence");
							userOutputFinal.setGeoOfferId(offerMapping.getGeoOfferId());
							userOutputFinal = serviceDao.getOfferIdByGeoCoordinate(userOutputFinal);
							userOutputFinal.setResponseStatus("IN_GEOFENCE");
							break;
						}else{
							userOutputFinal.setResponseStatus("NOT_IN_GEOFENCE");
						}
						
					} else {
						ArrayList<String> geoCoordinatesList = new ArrayList<>();
						StringTokenizer stringTokenizer = new StringTokenizer(offerMapping.getPolygonCoordinate(), "|");
						while (stringTokenizer.hasMoreElements()) {

							geoCoordinatesList.add(stringTokenizer.nextToken());

						}
						Boolean inFence = geofenceClass.isPersonInFence(geoX, geoY, geoCoordinatesList);
						if (inFence) {
							ServiceLogger.printDebugLog("in geoFence");
							userOutputFinal.setGeoOfferId(offerMapping.getGeoOfferId());
							userOutputFinal = serviceDao.getOfferIdByGeoCoordinate(userOutputFinal);
							userOutputFinal.setResponseStatus("IN_GEOFENCE");
							break;
						} else {
							userOutputFinal.setResponseStatus("NOT_IN_GEOFENCE");
						}

					}
				}
			} else {
				userOutputFinal.setResponseStatus("NOT_IN_GEOFENCE");
			}
		} else {
			UserOutputFinal bankOffers3 = new UserOutputFinal();
			bankOffers3.setResponseStatus("INVALID_COORDINATES");
			userOutputFinal = bankOffers3;

		}
		ServiceLogger.printDebugLog("OutPut:" + userOutputFinal.toString());
		return userOutputFinal;

	}

	@Override
	public List<CitiBankOffers> getMerchantOffers(Integer merchantId) {
		ServiceLogger.printInfoLog("Entered getMerchantOffers");
		List<CitiBankOffers> returnOfferList = serviceDao.getMerchantOffers(merchantId);
		return returnOfferList;
	}

	@Override
	public LoginDetails login(LoginDetails logindetails) {
		ServiceLogger.printInfoLog("Entered login");
		return serviceDao.authenticateLogin(logindetails);

	}

	@Override
	public ResponseMessage insertOffers(GeoOfferDetails geoOfferdetails) {
		ServiceLogger.printInfoLog("Entered insertOffers");
		return serviceDao.insertOffers(geoOfferdetails);
	}

	@Override
	public List<CitiBankOffers> loadOffers(LoginDetails logindetails) {
		ServiceLogger.printInfoLog("Entered loadOffers");
		return serviceDao.loadOffers(logindetails);
	}

	@Override
	public List<String> getAPIKeys() {
		ServiceLogger.printInfoLog("Entered getAPIKeys");
		List<String> apiKeys = helperClass.getApiKey(serviceDao.getAPIKeys());

		return apiKeys;

	}

}
