package com.citiofferservices.utils;

import org.apache.log4j.Logger;

public class ServiceLogger {
	
	private static final Logger LOG = Logger.getLogger(ServiceLogger.class);
	
	public static void printErrorLog(String message,Throwable e){
		LOG.error(message, e);
	}
	
	public static void printDebugLog(String message){
		LOG.debug(message);
	}

	public static void printInfoLog(String message){
		LOG.info(message);
	}
	

}
