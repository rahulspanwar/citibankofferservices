package com.citiofferservices.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

/**
 * @author 400039
 *
 */

public class MailManager {

	/** Logger Object to create logs. */
	public final static Logger ERRORLOG = Logger.getLogger("errorLogger");

	/**
	 * Default constructor of MailManager
	 * 
	 */

	public MailManager() {
		super();
	}

	/**
	 * Method to used to send exception mail
	 * @throws IOException 
	 * 
	 */
	
	public void sendMail(Throwable exception,String methodName) throws IOException {
		ServiceLogger.printInfoLog("Entered sendMail");
		final Properties prop = new Properties() ;
		InputStream inputstream = Thread.currentThread().getContextClassLoader().getResourceAsStream("mail.properties");
		prop.load(inputstream);
		// Recipient's email ID needs to be mentioned.

		// Sender's email ID needs to be mentioned
		final String from = prop.getProperty("mail.workflow.from");
		
		// Get system properties
		Properties properties = System.getProperties();

		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", prop.getProperty("mail.server.hostname"));

		properties.put("mail.smtp.port", prop.getProperty("mail.server.port"));
		properties.put("mail.smtp.auth", "true");
		Session session = Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, prop.getProperty("mail.workflow.password"));
			}
		});

		try {
			String msg = prop.getProperty("mail.workflow.bodyfirstException") +methodName+": " + exception.fillInStackTrace().toString()
					+ prop.getProperty("mail.workflow.bodylastException");
			/*String msg = prop.getProperty("mail.workflow.bodylastException")+exception + "\n\n\n"
					+ prop.getProperty("mail.workflow.bodylastException");  */
			
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(prop.getProperty("mail.workflow.to")));
			
			message.addRecipient(Message.RecipientType.CC, new InternetAddress(prop.getProperty("mail.workflow.cc")));

			// Set Subject: header field
			message.setSubject("Exception occured in CitiBank Offer Services");

			// Now set the actual message
			BodyPart messageBodyPart = new MimeBodyPart() ;
			
			messageBodyPart.setContent(msg , "text/html");
			
			Multipart multipart = new MimeMultipart() ;
			multipart.addBodyPart(messageBodyPart);
			
			message.setContent(multipart);
			
			// Send message
			Transport.send(message);
			ServiceLogger.printInfoLog("Sent message successfully....");
		} catch (Exception currEx) {
			ServiceLogger.printErrorLog("Exception Occured in Mail() " + currEx.getClass() + " "
					+ currEx.fillInStackTrace().toString(), currEx.fillInStackTrace());
		}finally{
			if(inputstream!=null)
				inputstream.close();
		}

	}
	

}
