package com.citiofferservices.utils;

import java.util.ArrayList;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.citiofferservices.data.UserOutputFinal;

@Repository
@Scope("prototype")
public class GeofenceClass {
	
	public static final double PI = 3.14159265;
	public static final double TWOPI = 2 * PI;
	ArrayList<Double> lat_array = new ArrayList<>();
	ArrayList<Double> long_array = new ArrayList<>();
	 Double destinationX;
	 Double destinationY;
	
	UserOutputFinal userOutputFinal = new UserOutputFinal();
	
	public boolean isPersonInFence(Double input1, Double input2, ArrayList<String> polygon_lat_long_pairs) {
		
		ServiceLogger.printInfoLog("polygon_lat_long_pairs :"+polygon_lat_long_pairs);
		
		ServiceLogger.printInfoLog("Input to isPersonInFence :" +"input1 :"+input1+"input2 :"+input2+"polygon_lat_long_pairs :"+polygon_lat_long_pairs);

		for (String s : polygon_lat_long_pairs) {
			lat_array.add(Double.parseDouble(s.split(",")[0]));
			long_array.add(Double.parseDouble(s.split(",")[1]));
		}

		coordinate_is_inside_polygon(input1, input2, lat_array, long_array);
		
		if (coordinate_is_inside_polygon(input1, input2, lat_array, long_array) == true) {
			return true;	
          
		} else {
			return false ;

		}
		
    
	}

	private boolean coordinate_is_inside_polygon(double latitude, double longitude, ArrayList<Double> lat_array,
			ArrayList<Double> long_array) {
		ServiceLogger.printInfoLog("lat_array:" +lat_array.toString());
		ServiceLogger.printInfoLog("long_array:" +long_array.toString());
		int i;
		double angle = 0;
		double point1_lat;
		double point1_long;
		double point2_lat;
		double point2_long;
		int n = lat_array.size();

		for (i = 0; i < n; i++) {
			point1_lat = lat_array.get(i) - latitude;
			point1_long = long_array.get(i) - longitude;
			point2_lat = lat_array.get((i + 1) % n) - latitude;
		
			point2_long = long_array.get((i + 1) % n) - longitude;
			angle += Angle2D(point1_lat, point1_long, point2_lat, point2_long);
		}

		if (Math.abs(angle) < PI)
			return false;
		else
			return true;
	}

	private double Angle2D(double y1, double x1, double y2, double x2) {
		double dtheta, theta1, theta2;

		theta1 = Math.atan2(y1, x1);
		theta2 = Math.atan2(y2, x2);
		dtheta = theta2 - theta1;
		while (dtheta > PI)
			dtheta -= TWOPI;
		while (dtheta < -PI)
			dtheta += TWOPI;

		return (dtheta);
	}
	
   public double getGeoFenceByDistance(Double input1, Double input2, String lat_long_pairs)
   {

		
			
			 destinationX =Double.parseDouble(lat_long_pairs.split(",")[0]);
			 destinationY=Double.parseDouble(lat_long_pairs.split(",")[1]);
		
		
		 return	distanceCalculate(input1, input2,destinationX,destinationY);  
	   
   }
	
	    private  double  distanceCalculate(Double input1,Double input2,Double destinationX,Double destinationY) 
	    {
	    	
        final int R = 6371; // Radius of the earth
        
        Double latDistance = Math.toRadians(destinationX - input1);
        Double lonDistance = Math.toRadians(destinationY - input2);

        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                     + Math.cos(Math.toRadians(input1))
                     * Math.cos(Math.toRadians(input2)) * Math.sin(lonDistance / 2)
                     * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c;
        return  distance;
        
        } 
              
}



