package com.citiofferservices.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.citiofferservices.data.Coordinates;
import com.citiofferservices.data.LocationDetails;

@Repository
public class HelperClass {

	@Value("${Offers.googleUrlpart1}")
	private String googleUrlpart1;

	@Value("${Offers.googleUrlpart2}")
	private String googleUrlpart2;

	@Value("${Offers.googleUrlAddress}")
	private String googleUrlAddress;

	@Value("${Offers.googleUrlApiKey1}")
	private String googleUrlApiKey1;

	@Value("${Offers.googleUrlApiKey2}")
	private String googleUrlApiKey2;

	@Value("${Offers.googleUrlApiKey3}")
	private String googleUrlApiKey3;

	@Value("${Offers.googleUrlApiKey4}")
	private String googleUrlApiKey4;

	public Coordinates distance(Coordinates coordinates, String maxdistance) {
		Integer maxDistance = Integer.parseInt(maxdistance);
		final int R = 6371; // Radius of the earth
		Double latDistance = Math.toRadians(coordinates.getDesLatitude() - coordinates.getSrcLatitude());
		Double lonDistance = Math.toRadians(coordinates.getDesLongitude() - coordinates.getSrcLongitude());

		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
				+ Math.cos(Math.toRadians(coordinates.getSrcLatitude()))
				* Math.cos(Math.toRadians(coordinates.getDesLatitude())) * Math.sin(lonDistance / 2)
				* Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c;

		double height = coordinates.getSrcHeight() - coordinates.getDesHeight();
		distance = Math.pow(distance, 2) + Math.pow(height, 2);
		distance = Math.sqrt(distance);
		if (distance > maxDistance) {
			coordinates.setDistance("-1");
			return coordinates;
		} else {
//			Coordinates cordinate = this.apiCallForDistance(coordinates, api);

			coordinates.setDistance(String.valueOf((distance)));
			return coordinates;
		}
	}

	public static Coordinates coordinatesSeparation(String coordinate) {
		Coordinates coordinates = new Coordinates();
		String[] coordinatesSplit = coordinate.split(",");
		coordinates.setSrcLatitude(Double.parseDouble(coordinatesSplit[0]));
		coordinates.setSrcLongitude(Double.parseDouble(coordinatesSplit[1]));
		coordinates.setDesHeight(0.0);
		coordinates.setSrcHeight(0.0);
		return coordinates;
	}

	public Coordinates apiCallForDistance(Coordinates coordinates, String apiKey) {
		try {
			// Proxy to be added for cognizant server


//			  System.getProperties().put("https.proxyHost",
//			  "proxy.cognizant.com");
//			  System.getProperties().put("https.proxyPort", "6050");
//			  System.getProperties().put("https.proxyUser", "SDBIDC");
//			  System.getProperties().put("https.proxyPassword", "BFSDW123#");
//			  System.setProperty("java.net.useSystemProxies", "true");


			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				@Override
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}

			} };

			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			
			String source = coordinates.getSrcLatitude() + "," + coordinates.getSrcLongitude();
			String destination = coordinates.getDesLatitude() + "," + coordinates.getDesLongitude();
			URL url = new URL(googleUrlpart1 + source + googleUrlpart2 + destination + "&key="+apiKey);

//			ServiceLogger.printInfoLog("URL:" + url);

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			StringBuilder responseStr = new StringBuilder();
			while ((output = br.readLine()) != null) {
				String response = "";
				responseStr = responseStr.append(response).append(output);
			}
			conn.disconnect();
//			ServiceLogger.printDebugLog("Api call : apiCallForDistance response :- " + responseStr);
			coordinates.setDistance(parseResponseJson(responseStr.toString()));
			coordinates.setResponseStatus(parseStatus(responseStr.toString()));
		} catch (Exception e) {

			ServiceLogger.printErrorLog("Exception occured in" + "apiCallForDistance", e);

		}
		return coordinates;

	}

	public static String parseStatus(String response)
	{
		JSONObject responseJson = new JSONObject(response);
		String status=responseJson.getString("status");
		if ("OK".equalsIgnoreCase(status))
		{
			status="API_SUCCESS";
		}
		else if ("REQUEST_DENIED".equalsIgnoreCase(status))
		{
			status="API_INVALID_KEY";
		}
		else
		{
			status="API_KEY_EXPIRED";
		}
		return status;

	}
	public static String parseResponseJson(String response) {

		JSONObject responseJson = new JSONObject(response);

		JSONArray resJson = responseJson.getJSONArray("rows");
		JSONObject innerJson = resJson.getJSONObject(0);
		JSONArray elementJson = innerJson.getJSONArray("elements");
		JSONObject distanceJson = elementJson.getJSONObject(0);
		JSONObject distJson = distanceJson.getJSONObject("distance");
		String distance = distJson.getString("text");
		return distance;
	}

	public LocationDetails apiCallForAddress(String coordinates, Map<String, String> apiMap) {
		LocationDetails locationDetails = new LocationDetails();
		try {
			StringBuilder responseStr = null;
			List<String> apiKeyList = getApiKey(apiMap);
			for (String string : apiKeyList) {
				String apiKey = string;
				// Proxy to be added for cognizant server


//				  System.getProperties().put("https.proxyHost",
//				  "proxy.cognizant.com");
//				  System.getProperties().put("https.proxyPort", "6050");
//				  System.getProperties().put("https.proxyUser", "SDBIDC");
//				  System.getProperties().put("https.proxyPassword",
//				  "BFSDW123#"); System.setProperty("java.net.useSystemProxies",
//				  "true");


				TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
					@Override
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return null;
					}

					@Override
					public void checkClientTrusted(X509Certificate[] certs, String authType) {
					}

					@Override
					public void checkServerTrusted(X509Certificate[] certs, String authType) {
					}

				} };

				SSLContext sc = SSLContext.getInstance("SSL");
				sc.init(null, trustAllCerts, new java.security.SecureRandom());
				HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

				// Create all-trusting host name verifier
				HostnameVerifier allHostsValid = new HostnameVerifier() {
					@Override
					public boolean verify(String hostname, SSLSession session) {
						return true;
					}
				};

				HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
				
				URL url = new URL(googleUrlAddress + coordinates +  "&key="+apiKey);
				ServiceLogger.printInfoLog("URL:" + url);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");

				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
				}

				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

				String output;
				responseStr = new StringBuilder();
				while ((output = br.readLine()) != null) {
					String response = "";
					responseStr = responseStr.append(response).append(output);
				}
				conn.disconnect();

				locationDetails = parseResponseJsonAddress(responseStr.toString());
				if (locationDetails != null) {
					break;
				}
			}

		} catch (Exception e) {

			ServiceLogger.printErrorLog("Exception occured in" + "apiCallForAddress", e);


		}
		return locationDetails;

	}

	public List<String> getApiKey(Map<String, String> apiMap) {
		List<String> apiKeyList = new ArrayList<String>();
		for (Map.Entry<String, String> entry : apiMap.entrySet()) {
			apiKeyList.add(entry.getValue());
		}

		/*
		 * apiKeyList.add(googleUrlApiKey1); apiKeyList.add(googleUrlApiKey2);
		 * apiKeyList.add(googleUrlApiKey3); apiKeyList.add(googleUrlApiKey4);
		 */
		return apiKeyList;
	}

	private static LocationDetails parseResponseJsonAddress(String response) {
		ServiceLogger.printInfoLog("response:" + response);

		LocationDetails locationDetails = new LocationDetails();
		String sublocality_level_1 = null;
		String administrative_area_level_2 = null;
		JSONObject responseJson = new JSONObject(response);
		if (responseJson.has("error_message")) {
			String errMsg = responseJson.getString("error_message");
			if (errMsg.equalsIgnoreCase("You have exceeded your daily request quota for this API.")) {
				ServiceLogger.printDebugLog("ErrorMessage " + errMsg);
				locationDetails.setStatusMsg("API_KEY_EXPIRED");
			}
			 if (responseJson.get("status").equals("REQUEST_DENIED")) {
				 locationDetails.setStatusMsg("REQUEST_DENIED");
			}
		} else if (responseJson.get("status").equals("ZERO_RESULTS")) {
			locationDetails.setStatusMsg("ZERO_RESULTS");
		} else {
			JSONArray resJson = responseJson.getJSONArray("results");
			JSONObject innerJson = resJson.getJSONObject(0);
			JSONArray addressJson = innerJson.getJSONArray("address_components");
			ServiceLogger.printInfoLog("addressJson:" + addressJson.toString());
			for (int i = 0; i < addressJson.length(); i++) {
				JSONObject addressinnerjson = addressJson.getJSONObject(i);
				JSONArray typesarray = addressinnerjson.getJSONArray("types");
				for (Object object : typesarray) {
					if (object.toString().equals("sublocality_level_1")) {
						sublocality_level_1 = addressinnerjson.getString("long_name");
					}
					if (object.toString().equals("administrative_area_level_2")) {
						administrative_area_level_2 = addressinnerjson.getString("long_name");
					}
				}
			}
			locationDetails.setLocation(sublocality_level_1);
			locationDetails.setArea(administrative_area_level_2);
			locationDetails.setStatusMsg("API_SUCCESS");
		}
		

		ServiceLogger.printInfoLog("administrative_area_level_2:" + administrative_area_level_2 + ","
				+ "sublocality_level_1:" + sublocality_level_1);
		return locationDetails;
	}
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
}
