package com.citiofferservices.utils;

import java.io.FileReader;
import java.util.Properties;



import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;





public class CommonDomain {

	private static String baseUrl = null;

	static {
		JSONParser parser = new JSONParser();
		Object obj;
		try {
			Properties properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("CitiOfferService.properties"));
			String path = System.getenv("TOMCAT_HOME") + properties.getProperty("Offers.baseUrlConfigFile");
			obj = parser.parse(new FileReader(path));
			JSONObject jsonObject = (JSONObject) obj;
			baseUrl = (String) jsonObject.get("currentDomain");
		} catch (Exception e) {
			ServiceLogger.printErrorLog("Exception occured in" + "CommonDomain", e);
		}

	}
	
	public static String returnBaseUrl()
	{
		return baseUrl ;
	}
}
