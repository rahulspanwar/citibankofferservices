package com.citiofferservices.utils;

public class Utility {
	static String configurationFile;

	public static String getConfigurationFile() {
		return configurationFile;
	}

	public static void setConfigurationFile(String configurationFile) {
		Utility.configurationFile = configurationFile;
	}
	
}
