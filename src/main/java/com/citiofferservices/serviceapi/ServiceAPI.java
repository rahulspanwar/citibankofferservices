package com.citiofferservices.serviceapi;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citiofferservices.data.APIKeys;
import com.citiofferservices.data.CitiBankOffers;
import com.citiofferservices.data.GeoOfferDetails;
import com.citiofferservices.data.InsertOffersCategorywise;
import com.citiofferservices.data.LocationDetails;
import com.citiofferservices.data.LoginDetails;
import com.citiofferservices.data.ResponseMessage;
import com.citiofferservices.service.IServiceManager;
import com.citiofferservices.utils.ServiceLogger;
import com.citiofferservices.data.UserInputFinal;
import com.citiofferservices.data.UserOutputFinal;

@Component
@Path("/services")
public class ServiceAPI {

	@Autowired(required = true)
	IServiceManager serviceManager;

	@GET
	@Path("/welcome")
	@Produces(MediaType.APPLICATION_JSON)
	public String display() {
		return "Welcome to citiBankServices";
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/offerDetails")
	public List<CitiBankOffers> getCitiBankOffer(final LocationDetails locationDetails) {
		ServiceLogger.printInfoLog("Entered getCitiBankOffer");
		return this.serviceManager.getCitiBankOffer(locationDetails);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/address/{location}")
	public LocationDetails getUserAddress(@PathParam("location") final String location) {
		ServiceLogger.printInfoLog("Entered getUserAddress");
		return this.serviceManager.getUserAddress(location);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/offers")
	public CitiBankOffers getOffer(final LocationDetails locationDetails) {
		ServiceLogger.printInfoLog("Entered getOffer");
		return this.serviceManager.getOffer(locationDetails);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/cardoffers")
	public List<CitiBankOffers> getCardOffers(final LocationDetails locationDetails) {
		ServiceLogger.printInfoLog("Entered getCardOffers");
		return this.serviceManager.getCardOffers(locationDetails);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/alloffers")
	public List<CitiBankOffers> getAllOffers(final LocationDetails locationDetails) {
		ServiceLogger.printInfoLog("Entered getAllOffers");
		return this.serviceManager.getAllOffers(locationDetails);
	}

	@POST
	@Path("/checkUserInGeoFenceByDistance")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public UserOutputFinal getUserDetailsByDistance(UserInputFinal userInputFinal) {
		ServiceLogger.printInfoLog("Entered getUserDetailsByDistance");

		return serviceManager.getUserDetailsByDistance(userInputFinal);

	}

	@GET
	@Path("/merchantoffers/{merchantId}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CitiBankOffers> getMerchantOffers(@PathParam("merchantId") final Integer merchantId) {
		ServiceLogger.printInfoLog("Entered getMerchantOffers");
		return this.serviceManager.getMerchantOffers(merchantId);
	}

	@POST
	@Path("/merchantlogin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public LoginDetails login(LoginDetails logindetails) {
		ServiceLogger.printInfoLog("Entered login");
		return this.serviceManager.login(logindetails);
	}

	@POST
	@Path("/insertoffers")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseMessage insertOffers(GeoOfferDetails geoOfferdetails) {
		ServiceLogger.printInfoLog("Entered insertOffers");
		return this.serviceManager.insertOffers(geoOfferdetails);
	}

	@POST
	@Path("/insertedofferscategorywise")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseMessage merchantInsertedOffers(InsertOffersCategorywise insertOffersCategorywise) {
		ServiceLogger.printInfoLog("Entered merchantInsertedOffers");
		ResponseMessage response = new ResponseMessage();
		response.setMessage("Offers successfully mapped..");
		response.setStatus(true);
		return response;
	}

	@POST
	@Path("/loadoffers")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<CitiBankOffers> loadOffers(LoginDetails logindetails) {
		ServiceLogger.printInfoLog("Entered loadOffers");
		return this.serviceManager.loadOffers(logindetails);
	}

	@GET
	@Path("/apiKeys")
	@Produces(MediaType.APPLICATION_JSON)
	public APIKeys getApiKeys() {
		ServiceLogger.printInfoLog("Entered getApiKeys");
		APIKeys apiKeys = new APIKeys();
		apiKeys.setApiKeys(this.serviceManager.getAPIKeys());
		return apiKeys;
	}

}
